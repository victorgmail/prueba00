from crypt import methods
import re
from flask import Flask, render_template, redirect, url_for, request, flash
from datetime import date, datetime
from flaskext.mysql import MySQL


app = Flask(__name__)

app.secret_key = 'clave_secreta_flask'

#configuracion de mySQL

app.config['MYSQL_DATABASE_HOST'] = '192.168.0.12'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'JtBbS3ui*uXH'
app.config['MYSQL_DATABASE_DB'] = 'practica1'

mysql = MySQL(app)

#context processors

@app.context_processor
def date_now():
    return{
        'now': datetime.utcnow()
    }
    

#endpoints

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/articulos", methods=['GET','POST'])
@app.route("/articulos/<int:articulo_id_edit>", methods=['GET','POST'])
def articulos(articulo_id_edit=None):

    mensaje_flash = "no"
    connect = mysql.connect()
    cursor= connect.cursor()
    articulo_edit=None
    articulo_edit_name=None
    articulo_edit_value=None
    articulo_edit_id=None

    """el siguiente metodo condicionado es para ubicar el contenido actual de un campo
    en los labels de edicion solamente cuando exista una id de edicion"""
    if articulo_id_edit!=None:          
        cursor.execute(f"SELECT * FROM articulos WHERE id = {articulo_id_edit}")
        articulo_edit = cursor.fetchall()
        if request.method=='POST':
            producto = request.form['producto']
            valor = request.form['valor']
        articulo_edit_id=int(articulo_edit[0][0])
        articulo_edit_name=articulo_edit[0][1]
        articulo_edit_value=articulo_edit[0][2]
    else:
        if request.method=='POST':
            confirmar=request.form['confirmar']
            if confirmar=="save":
                producto = request.form['producto']
                valor = request.form['valor']
                if producto!="" and valor!="":
                    cursor.execute("INSERT INTO articulos VALUES(NULL, %s, %s)",(producto, valor))
                    cursor.connection.commit()
                    mensaje_flash = "cargado"
                elif producto!="" and valor=="":
                    mensaje_flash="error_valor"
                elif producto=="" and valor!="":
                    mensaje_flash="error_producto"
                else:
                    mensaje_flash= "error"

            elif confirmar=="edit":
                producto = request.form['producto']
                valor = request.form['valor']
                id_art = int(request.form['id'])
                if producto!="" and valor!="":
                    cursor.execute("UPDATE articulos SET producto = %s,valor = %s WHERE id = %s",(producto, valor, id_art))
                    cursor.connection.commit()
                elif producto!="" and valor=="":
                    mensaje_flash="error_valor"
                elif producto=="" and valor!="":
                    mensaje_flash="error_producto"
                else:
                    mensaje_flash= "error"

    cursor.execute("SELECT * FROM articulos")
    articulos = cursor.fetchall()
    cursor.close()
    return render_template('articulos.html', 
                            mensaje_flash=mensaje_flash,
                            articulos=articulos,
                            articulo_edit_name=articulo_edit_name,
                            articulo_edit_value=articulo_edit_value,
                            articulo_edit_id=articulo_edit_id,
                            articulo_id_edit=articulo_id_edit
                            )
    

@app.route('/borrar-articulo/<int:articulo_id>')
def articulo_borrar(articulo_id):
    connect = mysql.connect()
    cursor= connect.cursor()
    cursor.execute(f"DELETE FROM articulos WHERE id = {articulo_id}")
    cursor.connection.commit()
    return redirect(url_for('articulos'))

@app.route('/editar-articulo/<int:articulo_id>', methods=['GET','POST'])
def articulo_editar(articulo_id):
    connect = mysql.connect()
    cursor= connect.cursor()
    cursor.execute("SELECT id FROM articulos WHERE id = %s", (articulo_id))
    articulo=cursor.fetchall()
    cursor.close()
    return redirect(url_for('articulos',articulo_id_edit=articulo[0][0]))

@app.route("/pedidos", methods=['GET','POST'])
def pedidos():
    connect = mysql.connect()
    cursor= connect.cursor()
    calculate=False
    calculate_sign=False
    msg="no"
    nombreCliente = ""
    datetime= ""
    direccion = ""
    ciudad = ""
    telefono = ""
    id_util = ""
    cantidad = ""
    tot_valor = ""
    if request.method=='POST':
        nombreCliente = request.form['nombreCliente']
        datetime=request.form['datetime']
        direccion = request.form['direccion']
        ciudad = request.form['ciudad']
        telefono = request.form['telefono']
        try:
           id_util = int(request.form['entrada'])
        except:
            id_util = ""
        cantidad = request.form['cantidad']
        if id_util!="":
            if cantidad!="":
                if float(cantidad) > 0:
                    cursor.execute(f"SELECT valor FROM articulos WHERE id={id_util}")
                    ind_valor=cursor.fetchall()
                    tot_valor=float(cantidad)*ind_valor[0][0]
                else:
                    msg = "zero_negative"
                    tot_valor = ""
            else:
                msg = "noquantity"
                tot_valor = ""
        else:
            msg = "noproduct"
            tot_valor = ""
            
        if request.form['confirmar']=="calculate":
            calculate=True
            calculate_sign=True
            if id_util=="" or cantidad=="" or float(cantidad) < 1:
                calculate_sign=False
            cursor.execute("SELECT * FROM articulos")
            articulos=cursor.fetchall()
            cursor.close()
            return render_template('pedidos.html',
                                    articulos=articulos,
                                    datetime=datetime,
                                    nombreCliente=nombreCliente,
                                    direccion=direccion,
                                    ciudad=ciudad,
                                    telefono=telefono,
                                    cantidad=cantidad,
                                    calculate=calculate,
                                    tot_valor=tot_valor,
                                    id_util=id_util,
                                    msg=msg,
                                    calculate_sign=calculate_sign
                                    )
        if request.form['confirmar']=="add": #cadena de condicionales
            msg="ready"
            if nombreCliente!="":
                if datetime!="":
                    if direccion!="":
                        if ciudad!="":
                            if telefono!="":
                                if id_util!="":
                                    if cantidad!="":
                                        if float(cantidad) > 0:               
                                            cursor.execute(f"INSERT INTO form_pedidos VALUES(NULL, '{nombreCliente}', '{direccion}', '{datetime}', '{ciudad}', '{telefono}', '{id_util}', '{id_util}', '{cantidad}', '{tot_valor}', 'backordered')")
                                            cursor.connection.commit()
                                        else:
                                            msg="zero_negative"
                                    else:
                                        msg="error_cantidad"
                                        calculate=True
                                else:
                                    msg="error_id"
                                    calculate=True
                            else:
                                msg="error_telefono"
                                calculate=True
                        else:
                            msg="error_ciudad"
                            calculate=True
                    else:
                        msg="error_direccion"
                        calculate=True
                else:
                    msg="error_datetime"
                    calculate=True
            else:
                msg="error_nombrecliente"
                calculate=True
            
            if id_util!="" and cantidad!="" and float(cantidad) > 0:
                calculate_sign=True
    
    cursor.execute("SELECT * FROM articulos")
    articulos=cursor.fetchall()
    cursor.close()
    return render_template('pedidos.html',
                            articulos=articulos,
                            datetime=datetime,
                            nombreCliente=nombreCliente,
                            direccion=direccion,
                            ciudad=ciudad,
                            telefono=telefono,
                            cantidad=cantidad,
                            calculate=calculate,
                            tot_valor=tot_valor,
                            id_util=id_util,
                            msg=msg,
                            calculate_sign=calculate_sign
                            )
    

@app.route("/estado_pedidos", methods=['GET','POST'])
def estado_pedidos():
    connect = mysql.connect()
    cursor= connect.cursor()
    msg="no"
    if request.method=='POST':
        id_pedido=request.form['actualizar']
        status=request.form[f'status{id_pedido}']
        fech_hora=request.form[f'fech_hora{id_pedido}']
        if fech_hora!="":
            cursor.execute("UPDATE form_pedidos SET estado=%s, tiempo=%s WHERE id=%s", (status, fech_hora, id_pedido))
            cursor.connection.commit()
        else:
            msg=id_pedido


    cursor.execute("SELECT form_pedidos.id, form_pedidos.nombre, form_pedidos.direccion, form_pedidos.tiempo, form_pedidos.ciudad, form_pedidos.telefono, articulos.producto, form_pedidos.cantidad, form_pedidos.tot_valor, form_pedidos.estado, form_pedidos.cod_producto FROM form_pedidos INNER JOIN articulos ON form_pedidos.cod_producto = articulos.id")
    pedidos=cursor.fetchall()
    cursor.close()
    return render_template('estado_pedidos.html',
                            pedidos=pedidos,
                            msg=msg
                            )

@app.route("/borrar_pedido/<int:id_pedido>")
def borrar_pedido(id_pedido):
    connect = mysql.connect()
    cursor = connect.cursor()
    cursor.execute(f"DELETE FROM form_pedidos WHERE id={id_pedido}")
    cursor.connection.commit()
    return redirect(url_for('estado_pedidos'))

if __name__ == '__main__': #se valida que estemos desde la pagina principal
    app.run(port=5000, debug=True, host='0.0.0.0') #el metodo run permite ejecutar nuestra aplicacion. se pueden dar mas configuraciones pero con esto funcionara
